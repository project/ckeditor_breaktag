
## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Minimal module to insert a break tag into the content by using
the provided button or using the keyboard shortcut Ctrl+Enter.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ckeditor_breaktag

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/ckeditor_breaktag


## REQUIREMENTS

Requires CKEditor5 module


## RECOMMENDED MODULES

No special Modules are required.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

 * Go to the 'Text formats and editors' configuration page:
   /admin/config/content/formats, and for each text
   format/editor combo where you want to use BreakTag,
   drag and drop the 'BreakTag' icon to the admin toolbar.


## MAINTAINERS

Current maintainers:
 * Chennakesavulu Chakka (chakkche) - https://www.drupal.org/u/chakkche
