import DrupalBreakTag from './drupalbreaktag';

/**
 * @private
 */
export default {
  DrupalBreakTag,
};
