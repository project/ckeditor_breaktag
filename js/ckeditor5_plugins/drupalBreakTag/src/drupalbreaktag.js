import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import breakTagIcon from '../../../../icons/linebreak.svg';

class DrupalBreakTag extends Plugin {

  /**
   * @inheritdoc
   */
  init() {
    const editor = this.editor;
    editor.ui.componentFactory.add('linebreak', (locale) => {
      const button = new ButtonView(locale);
      const shiftenterCommand = editor.commands.get('shiftEnter');

      // Button Ui.
      button.set({
        label: Drupal.t('Insert Line Break (Shift+Enter)'),
        icon: breakTagIcon,
        tooltip: true,
        isToggleable: true
      });

      // Bind button to the command.
      button.bind('isOn', 'isEnabled').to(shiftenterCommand, 'value','isEnabled');

      // Execute shiftEnter command to insert the break tag.
      this.listenTo(button, 'execute', () => {
        editor.execute('shiftEnter');
        editor.editing.view.focus();
      });

      return button;
    });
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'DrupalBreakTag';
  }
}

export default DrupalBreakTag;
